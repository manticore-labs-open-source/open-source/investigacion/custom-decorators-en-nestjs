# Decoradores de ruta personalizados

Un decorador  es una expresión que devuelve una función y puede tomar un objetivo, nombre y descriptor de propiedad como argumentos. Los decoradores pueden definirse para una clase o una propiedad.

Nest proporciona un conjunto de decoradores de parámetros útiles que puede utilizar junto con los controladores de ruta HTTP. A continuación se muestra una comparación de los decoradores con los objetos expresos planos.

![](./imagenes/Selección_187.png)


En el mundo node.js, es una práctica común adjuntar propiedades al objeto de solicitud . Luego, estos deben ser capturardos manualmente cada vez que se utilice los controladores de ruta, por ejemplo:

``` JavaScript
const user = req.user;
```

Para hacerlo más legible y transparente, podemos crear un @User()decorador y reutilizarlo en todos los controladores existentes.

``` JavaScript

import { createParamDecorator } from '@nestjs/common';

export const User = createParamDecorator((data, req) => {
  return req.user;
});
```

Entonces, se puede usar donde sea que lo necesitemos.

``` JavaScript

@Get()
async findOne(@User() user: UserEntity) {
  console.log(user);
}
```

## Pasar datos
Cuando el comportamiento del decorador depende de algunas condiciones, puede usar el parámetro  data para pasar un argumento a la función del decorador. Por ejemplo:

``` JavaScript
@Get()
async findOne(@User('test') user: UserEntity) {
  console.log(user);
}
```

Será posible acceder a al string test  a través del argumento data:

``` JavaScript
import { createParamDecorator } from '@nestjs/common';

export const User = createParamDecorator((data, req) => {
  console.log(data); // test
  return req.user;
});
```

## Trabajando con pipes
Nest trata a los decoradores personalizados de la misma manera que los incorporados ( @Body(), @Param()y @Query()). Es decir se puede aplicar el pipe directamente al decorador personalizado:

``` JavaScript
@Get()
async findOne(@User(new ValidationPipe()) user: UserEntity) {
  console.log(user);
}
```
<a href="https://www.facebook.com/oscar.sambache" target="_blank"><img alt="Encuentrame en facebook:" height="35" width="35" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTANMlMcb_-pUSuzjnpvSynOA3Ontg9Z2I1NE24WQ_KAk34yYmh"/> Oscar Sambache</a>